from .global_settings import *

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = ['*']

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'biblioteca',
        'USER': 'postgres',                      # Not used with sqlite3.
        'PASSWORD': 'a',                  # Not used with sqlite3.
        'HOST': 'localhost',           # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '5432',                   # Set to empty string for default. Not used with sqlite3.
   }
}

# Password validation
# https://docs.djangoproject.com/en/2.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = []

EMAIL_SUBJECT_PREFIX = 'Bilioteca FATEC'

try:
    from .local_settings import *
except:
    pass