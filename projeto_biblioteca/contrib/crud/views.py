from django.views import generic
from django.forms import inlineformset_factory
from django.shortcuts import render
from django.urls import path
from django.conf.urls import url
from cruds_adminlte.crud import CRUDView
from cruds_adminlte import utils
from ..auth.mixins import LoginRequiredMixin
from apps.notificacao.utils import criar_mensagem, agendar_envio_mensagem, enviar_mensagem


class MyCRUDViewInlines(CRUDView):
    inlines_add_form = []
    inline_field = ''
    extra = 1

    def get_create_view(self):
        CreateView = super().get_create_view()

        class MyCreateView(CreateView):
            inlines_add_form = self.inlines_add_form
            parent = self.model
            inline_field = self.inline_field
            extra = self.extra
            # formsets = [] pensar em melhor no context data pra só chamar ele no render do else

            def form_valid(self, form):
                formsets = [inlineformset_factory(self.parent, klass.Meta.model, form=klass, min_num=1)
                            (data=self.request.POST, instance=None, prefix=klass.Meta.prefix)
                            for klass in self.inlines_add_form]
                if all([formset.is_valid() for formset in formsets]):
                    object = form.save()
                    for formset in formsets:
                        for item in formset:
                            if list(item.cleaned_data.values()):
                                a = item.save(commit=False)
                                setattr(a, self.inline_field, object)
                                a.save()
                    object.save()  # para caso tenha um pré save q dependa dos objetos relacionados
                else:
                    context = self.get_context_data()
                    context['inlines'] = [{'title': formset.model._meta.verbose_name_plural, 'formset': formset} for formset in formsets]
                    return render(self.request, self.template_name, context)
                return super().form_valid(form)

            def get_context_data(self, **kwargs):
                context = super().get_context_data(**kwargs)
                context['inlines'] = self.get_inlines_create()
                return context

            def get_inlines_create(self):
                formsets = [inlineformset_factory(self.parent, klass.Meta.model, form=klass, extra=self.extra)
                            (instance=None, prefix=klass.Meta.prefix) for klass in self.inlines_add_form]
                return [{'title': formset.model._meta.verbose_name_plural, 'formset': formset} for formset in formsets]
        return MyCreateView


class DashboardViewBase(LoginRequiredMixin, generic.TemplateView):
    app_name = None

    def get_urls(self):
        return [path('{}/'.format(self.app_name), self.__class__.as_view(), name='{}_index'.format(self.app_name))]


class HomeView(LoginRequiredMixin, generic.TemplateView):
    template_name = 'adminlte/index.html'


