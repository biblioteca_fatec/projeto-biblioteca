from django.views.generic.edit import FormMixin
from apps.arquivos.models import Arquivo
from apps.imagens.models import Imagem


class FileCrudMixin(FormMixin):
    model = None
    object = None
    upload_arquivos = False
    upload_imagens = False

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({
            'user': self.request.user
        })
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        opts = self.model._meta
        context['model_name'] = opts.object_name
        context['app_label'] = opts.app_label
        context['upload_arquivos'] = self.upload_arquivos
        context['upload_imagens'] = self.upload_imagens
        if self.object and self.upload_arquivos:
            context['arquivos'] = Arquivo.objects.arquivos_do_objeto(self.object)
        if self.object and self.upload_imagens:
            context['imagens'] = Imagem.objects.arquivos_do_objeto(self.object)
        return context


