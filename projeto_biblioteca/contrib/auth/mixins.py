from django.contrib.auth.mixins import LoginRequiredMixin as DjangoLoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse_lazy


class LoginRequiredMixin(DjangoLoginRequiredMixin):
    login_url = reverse_lazy('login')
    redirect_field_name = 'redirect_to'


class LoginRequiredMessageMixin(LoginRequiredMixin, SuccessMessageMixin):
    pass


class ClienteRequiredMixin(LoginRequiredMixin):
    permission_denied_message = _("Acesso destinado apenas para Clientes!")
    cliente = None

    def dispatch(self, request, *args, **kwargs):
        self.cliente = request.user.is_cliente
        if not self.cliente:
            self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['cliente'] = self.cliente
        return context
