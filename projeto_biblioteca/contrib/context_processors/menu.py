from projeto_biblioteca.contrib.menu import Menu
from django.utils.translation import ugettext_lazy as _


def menu(request):
    main = []

    # LIVRO
    livro = [
        Menu(_('Livro'), 'livro_livro_list', request, icon='fa fa-tachometer-alt'),
        Menu(_('Autor'), 'livro_autor_list', request, icon=None),

    ]

    main.append(
        Menu(_('LIVRO'), 'livro_livro_list', request, livro, icon='fa fa-tachometer-alt')
    )

    treinamento = [
        Menu(_('Tutoriais'), 'treinamento_treinamento_list', request, icon='fa fa-tachometer-alt'),
    ]

    main.append(
        Menu(_('TUTORIAIS'), 'treinamento_treinamento_list', request, treinamento, icon='fa fa-tachometer-alt')
    )

    return {'menu': main}

