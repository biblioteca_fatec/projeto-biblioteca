from django.db import models


class URLFieldNoValidator(models.URLField):
    default_validators = []


class IndexField(models.CharField):

    def get_internal_type(self):
        return "IndexField"

    def db_type(self, connection):
        return 'char(%s)' % self.max_length
