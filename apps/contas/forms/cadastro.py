from django import forms
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.db import transaction
from ..models import PessoaFisica


class PessoaFisicaForm(forms.ModelForm):
    class Meta:
        model = PessoaFisica
        fields = ['first_name', 'last_name', 'email',  'rg', 'cpf', 'password', 'data_nascimento',]
        widgets = {
            'first_name': forms.TextInput(attrs={'placeholder': 'Nome', 'class': 'form-control'}),
            'last_name': forms.TextInput(attrs={'placeholder': 'Sobrenome', 'class': 'form-control'}),
            'email': forms.EmailInput(attrs={'placeholder': 'Email', 'class': 'form-control'}),
            'password': forms.PasswordInput(attrs={'placeholder': 'Senha', 'class': 'form-control'}),
        }

    confirm_password = forms.CharField(max_length=30, widget=forms.PasswordInput(attrs={'placeholder': _('Confirma Senha'),
                                                                                        'class': 'form-control'}))

    def clean(self):
        cleaned_data = super().clean()
        password = cleaned_data.get("password")
        confirm_password = cleaned_data.get("confirm_password")
        if password != confirm_password:
            self.add_error('confirm_password', _('As senhas informadas são diferentes! Verifique suas entradas.'))

    @transaction.atomic
    def save(self, commit=True):
        instance = super().save(commit=commit)
        instance.set_password(instance.password)
        instance.save()
        return instance


class PessoaBaseForm(forms.ModelForm):
    class Meta:
        model = None
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['email'].required = True

    def clean_email(self):
        email = self.cleaned_data['email']
        if self.instance.pk:
            qs = User.objects.exclude(pk=self.instance.pk).filter(email=email).exists()
        else:
            qs = User.objects.filter(email=email).exists()
        if qs:
            raise forms.ValidationError(_('O endereço informado já está sendo utilizado.'))
        return email

    def save(self, commit=True):
        instance = super().save(commit=False)
        if not instance.pk:
            instance.username = instance.email
            senha = User.objects.make_random_password(length=8)
            instance.set_password(senha)
        instance.save()
        return instance
