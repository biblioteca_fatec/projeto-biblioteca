from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.models import User
from django.utils.safestring import mark_safe
from apps.contas.models import PessoaFisica


class ResetarSenhaForm(forms.Form):
    senha = forms.CharField(label=_("Senha"), max_length=20, widget=forms.PasswordInput(attrs={'class': 'form-control'}), required=False)
    resetar = forms.BooleanField(widget=forms.CheckboxInput(attrs={'style': 'display: none'}), required=False)
    _pessoa = None

    def __init__(self, *args, **kwargs):
        self._pessoa = kwargs.pop('pessoa')
        super().__init__(*args, **kwargs)
        self.fields['resetar'].initial = True

    def clean(self):
        cleaned_data = super().clean()
        if not cleaned_data.get('resetar') and not cleaned_data.get('senha'):
            self.add_error('senha', _('Você precisa definir uma senha.'))
        return cleaned_data

    def resetar_senha(self):
        senha = User.objects.make_random_password(length=8) if self.cleaned_data['resetar'] else self.cleaned_data['senha']
        self._pessoa.set_password(senha)
        self._pessoa.save()
        mensagem = "{}: {}".format(_("Sua nova senha é"), senha)
        self._pessoa.notificar(_('Nova Senha'), mensagem)


class TrocarSenhaForm(PasswordChangeForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs.update({'class': 'form-control'})

    def trocar_senha(self, commit=True):
        senha = self.cleaned_data["new_password1"]
        self.user.set_password(senha)
        if commit:
            self.user.save()
            self.user.pessoa.notificar(_('Redefinição de Senha'), _('Sua senha foi redefinida com sucesso!'))
        return self.user


class RecuperarSenhaForm(forms.Form):
    _pessoa = None
    email = forms.EmailField(label=_("E-mail"), widget=forms.EmailInput(attrs={'tabindex': 1, 'class': 'form-control',
                                                                               'placeholder': _("Email")}))

    def redefinir_senha(self):
        nova_senha = PessoaFisica.objects.make_random_password(length=8).lower()
        self._pessoa.set_password(nova_senha)
        self._pessoa.save()
        #notifica senha senha modificada
        message = mark_safe(
            """
            Sua senha foi recuperada com sucesso!<br/><br/>Para acessar o sistema, utilize a nova senha %s<br><br>
            """ % nova_senha
        )
        self._pessoa.notificar(_("Senha recuperada!"), message)

    def clean_email(self):
        data = self.cleaned_data['email'].lower()
        self._pessoa = PessoaFisica.objects.filter(username=data).first()
        if not self._pessoa:
            raise forms.ValidationError(_("O endereço informado não foi encontrado em nosso sistema."))
        return data