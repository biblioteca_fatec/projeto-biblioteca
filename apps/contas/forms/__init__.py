from .cadastro import PessoaFisicaForm, PessoaBaseForm
from .login import LoginForm
from .senha import ResetarSenhaForm, TrocarSenhaForm, RecuperarSenhaForm
