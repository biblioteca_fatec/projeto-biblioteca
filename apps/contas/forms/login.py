from django import forms
from django.contrib.auth import authenticate
from django.utils.translation import ugettext_lazy as _
from django.forms.utils import ErrorList



class LoginForm(forms.Form):
    _user = None
    usuario = forms.CharField(label=_('Usuário'), widget=forms.TextInput(attrs={'tabindex': 1, 'class': 'form-control', 'placeholder': _('Email')}))
    senha = forms.CharField(label=_('Senha'), max_length=20, widget=forms.PasswordInput(attrs={'tabindex': 2, 'class': 'form-control', 'placeholder': _('Senha')}))

    def clean_username(self):
        return self.cleaned_data['username'].lower()

    def clean(self):
        cleaned_data = self.cleaned_data
        if 'usuario' in cleaned_data and 'senha' in cleaned_data:
            self._user = authenticate(username=cleaned_data['usuario'], password=cleaned_data['senha'])
            if not self._user:
                self._errors['usuario'] = ErrorList([_('Usuário e/ou senha inválido.')])
        return cleaned_data

    @property
    def user(self):
        return self._user
