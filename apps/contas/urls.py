from django.urls import path
from .views import *


urlpatterns = [
    path('acesso/login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('cadastro/', CadastroView.as_view(), name='cadastro'),
    path('alterar-senha/', TrocarSenhaView.as_view(), name='alterar_senha'),
    path('redefinir-senha/', RecuperarSenhaView.as_view(), name='redefinir_senha'),
    path('resetar-senha/<int:pessoa_id>/', ResetarSenhaView.as_view(), name='resetar_senha'),
]
