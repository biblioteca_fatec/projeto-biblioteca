from .pessoa import PessoaFisica
from .endereco import Endereco
from .pessoa_endereco import PessoaEndereco
from .pessoa_imagem import PessoaImagem
from .user import *
