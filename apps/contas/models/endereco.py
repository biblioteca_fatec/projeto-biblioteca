from django.db import models
from django.utils.translation import ugettext_lazy as _
from phonenumber_field.modelfields import PhoneNumberField


class Endereco(models.Model):
    contato = models.CharField(_("Contato"), max_length=100, blank=True, null=True)
    telefone = PhoneNumberField(_("Telefone"), max_length=100, blank=False, null=True)
    celular = PhoneNumberField(_("Celular"), max_length=100, blank=True, null=True)
    celular_2 = PhoneNumberField(_("Celular (2)"), max_length=100, blank=True, null=True)
    rua = models.CharField(_("Rua"), max_length=500, blank=True, null=True)
    numero = models.CharField(_("Número"), max_length=500, blank=True, null=True)
    bairro = models.CharField(_("Bairro"), max_length=100, blank=True, null=True)
    complemento = models.CharField(_("Complemento"), max_length=100, blank=True, null=True)
    estado = models.CharField(_("Estado"), max_length=100, blank=True, null=True)
    cidade = models.CharField(_("Cidade"), max_length=100, blank=False, null=True)
    cep = models.CharField(_("CEP"), max_length=100, blank=False, null=True)
    localizacao = models.CharField(_("Latidude e Longitude"), max_length=100, blank=True, null=True,
                                   help_text=_('Ex: -33.8569, 151.2152'))

    class Meta:
        verbose_name = _('Endereço')
        verbose_name_plural = _('Endereços')
        abstract = True

    def __str__(self):
        return '%s, %s, %s%s %s-%s' % (self.rua, self.bairro, ' %s ' % self.complemento if self.complemento else '',
                                       self.numero, self.cidade, self.estado)

