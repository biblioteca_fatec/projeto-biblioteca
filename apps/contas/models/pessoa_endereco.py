from django.db import models
from django.utils.translation import gettext as _
from apps.contas.models import Endereco


class PessoaEnderecoQuerySet(models.QuerySet):
    def contato_principal(self):
        return self.filter(tipo_endereco=PessoaEndereco.CONTATO_PRINCIPAL)

    def faturamento(self):
        return self.filter(tipo_endereco=PessoaEndereco.FATURAMENTO)

    def entrega(self):
        return self.filter(tipo_endereco=PessoaEndereco.ENTREGA)

    def correspondencia(self):
        return self.filter(tipo_endereco=PessoaEndereco.CORRESPONDENCIA)

    def cobranca(self):
        return self.filter(tipo_endereco=PessoaEndereco.COBRANCA)

    def padrao(self):
        return self.filter(default_address=True).first()


class PessoaEnderecoManager(models.Manager):
    def get_queryset(self):
        return PessoaEnderecoQuerySet(self.model, using=self._db)

    def endereco_contato(self):
        return self.get_queryset().contato_principal()


class PessoaEndereco(Endereco):
    objects = PessoaEnderecoManager()

    class Meta:
        verbose_name = _(u'Endereço')
        verbose_name_plural = _('Endereços')
        ordering = ('-endereco_padrao',)

    FATURAMENTO = 0
    CONTATO_PRINCIPAL = 1
    COBRANCA = 2
    CORRESPONDENCIA = 3
    ENTREGA = 4

    CHOICES = (
        (FATURAMENTO, _('Faturamento')),
        (CONTATO_PRINCIPAL, _('Contato/Principal')),
        (COBRANCA, _('Cobrança')),
        (CORRESPONDENCIA, _('Correspondência')),
        (ENTREGA, _('Entrega')),
    )

    usuario = models.ForeignKey('auth.User', on_delete=models.PROTECT)
    endereco_padrao = models.BooleanField(_('Endereço Padrão?'), default=False, help_text=_(
        'O endereço marcado como padrão será escolhido automaticamente no momento da compra ou assinatura.'))
    tipo_endereco = models.SmallIntegerField(
        _('Tipo'), choices=CHOICES, null=True, blank=True)
    publicar = models.BooleanField(_('Publicar Endereço?'), default=False)
    publicar_telefone = models.BooleanField(_('Publicar Telefone de Contato?'), default=False)

    def __str__(self):
        return '%s, %s, %s%s %s-%s' % (self.rua, self.bairro, ' %s ' %
                                          self.complemento if self.complemento else '', self.numero, self.cidade, self.estado)

    def inline(self):
        #TODO melhorar maneira de pegar o endereco unicode
        return '<< %s >> %s, %s, %s%s %s-%s' % (self.contato, self.rua, self.bairro, ' %s ' %
                                          self.complemento if self.complemento else '', self.numero, self.cidade, self.estado)

    def get_absolute_url(self):
        return None

    def e_entrega(self):
        return self.tipo_endereco == self.ENTREGA

    def e_cobranca(self):
        return self.tipo_endereco == self.COBRANCA

    @property
    def tipo_endereco_detalhado(self):
        for v, d in self.CHOICES:
            if self.tipo_endereco == v:
                return d
        return _('Tipo de endereço não encontrado')

