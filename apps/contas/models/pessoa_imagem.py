from django.db import models
from django.utils.translation import gettext as _


class PessoaImagem(models.Model):
    class Meta:
        verbose_name = _('Imagem')
        verbose_name_plural = _('Imagens')

    AGUARDANDO_AVALIACAO = 0
    APROVADO = 1
    DESAPROVADO = 2

    STATUS_CHOICES = [
        (AGUARDANDO_AVALIACAO, _(u"Aguardando avaliação")),
        (APROVADO, _(u"Aprovado")),
        (DESAPROVADO, _(u"Reprovado"))
    ]
    usuario = models.ForeignKey('auth.User', on_delete=models.PROTECT)
    imagem = models.ImageField(_("Imagem"), upload_to='person/images')
    status = models.SmallIntegerField(_("Status"), choices=STATUS_CHOICES, default=AGUARDANDO_AVALIACAO)

    def __str__(self):
        return self.pk