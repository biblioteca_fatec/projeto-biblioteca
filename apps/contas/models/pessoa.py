from django.db import models
from django.db.models import signals
from django.contrib.auth.models import User, UserManager
from django.core.exceptions import ValidationError
from django.utils.translation import gettext as _
from apps.core.utils import slug_sender_pre_save, ThumbnailMixin, BannerMixin
from apps.notificacao.utils import criar_mensagem
from django.shortcuts import reverse


class PessoaQuerySet(models.QuerySet):
    def ativos(self):
        return self.filter(is_active=True)


class PessoaManager(UserManager):
    def get_queryset(self):
        return PessoaQuerySet(self.model, using=self._db)


class Pessoa(User, ThumbnailMixin, BannerMixin):
    objects = PessoaManager()
    thumb_field = 'avatar'
    thumb_default = 'images/default/person.png'
    thumb_geometry = '224x224'

    class Meta:
        verbose_name = _('Pessoa')
        verbose_name_plural = _('Pessoa')
        abstract = True
        
    slug = models.SlugField(unique=True, null=True, blank=True)
    sobre = models.TextField(_("Sobre"), blank=True, default='')
    website = models.URLField(_('Website/URL'), blank=True, null=True)

    avatar = models.ImageField(_("Avatar, Logotipo ou Imagem Ilustrativa"), upload_to='pessoa/images',
                               blank=True, null=True, help_text=_('%s ou proporcional' % thumb_geometry))

    banner_image = models.ImageField(_("Capa/Banner"), upload_to='pessoa/images',
                                     blank=True, null=True, help_text=_('%s ou proporcional' %
                                                                        BannerMixin.banner_geometry))

    reset_password = models.BooleanField(_('Redefinir Senha'), default=False)

    def __str__(self):
        return self.first_name

    @property
    def slug_with_this(self):
        return str(self.id)

    def save(self, *args, **kwargs):
        self.username = self.email
        super().save(*args, **kwargs)

    def clean(self):
        if self.email:
            if User.objects.exclude(pk=self.pk).filter(email=self.email).exists():
                raise ValidationError({'email': _('O endereço informado já está sendo utilizado.')})

    @property
    def endereco_principal(self):
        raise Exception("Me faca!")

    def notificar(self, assunto, message, template=None, files=None):
        context = {
            'destinatario': self.razao_social if hasattr(self, 'razao_social') else self.first_name
        }
        mensagem = criar_mensagem(assunto, message, self.email, template=template, obj=self, files=files, context=context)
        return mensagem

    @property
    def is_pessoa_fisica(self):
        if hasattr(self, 'pessoafisica'):
            return self.pessoafisica
        return False

    @property
    def is_pessoa_juridica(self):
        if hasattr(self, 'pessoajuridica'):
            return self.pessoajuridica
        return False


    # def get_resetar_senha_url(self):
    #     return reverse('resetar_senha', kwargs={'user_id': self.user_ptr_id})


class PessoaFisica(Pessoa):
    class Meta:
        verbose_name = _('Pessoa Física')
        verbose_name_plural = _('Pessoas Físicas')

    cpf = models.CharField(_("CPF"), max_length=14, blank=False, null=True, unique=True)
    rg = models.CharField(_("RG"), max_length=9, blank=True, null=True, unique=True)
    passaporte = models.CharField(_("Passaporte"), max_length=15, blank=True, null=True, unique=True)
    data_nascimento = models.DateField(_("Data de Nascimento"), blank=False, null=True)
    
    MASCULINO = 'Masculino'
    FEMININO = 'Feminino'
    CHOICES_GENERO = [
        (MASCULINO, MASCULINO),
        (FEMININO, FEMININO)
    ]
    genero = models.CharField(_('Gênero'), max_length=100, choices=CHOICES_GENERO, blank=True, null=True)

    def __str__(self):
        return self.get_full_name()


# class PessoaJuridica(Pessoa):
#     class Meta:
#         verbose_name = _('Pessoa Jurídica')
#         verbose_name_plural = _('Pessoas Jurídicas')
#
#     nome = models.CharField(_("Razão Social"), max_length=200)
#     nome_fantasia = models.CharField(_("Nome Fantasia"), max_length=200, blank=True, null=True)
#     cnpj = models.CharField(_("CNPJ"), max_length=20, unique=True, help_text=_('Código de identificação internacional '
#                                                                                'da pessoa jurídica. Para Brasil utilize'
#                                                                                ' o CNPJ'))
#
#     inscricao_estadual = models.CharField(_("Inscrição Estadual"), max_length=200, blank=True, null=True)
#     inscricao_municipal = models.CharField(_("Inscrição Municipal"), max_length=200, blank=True, null=True)
#
#     def __str__(self):
#         return self.nome_fantasia or self.nome
#
#     @property
#     def site(self):
#         return self.website or ''


def get_impersonate_url(self):
    try:
        return reverse('impersonate-start', args=[self.id])
    except Exception as e:
        print(e)
        return None

User.add_to_class('get_impersonate_url', get_impersonate_url)
signals.pre_save.connect(slug_sender_pre_save, sender=Pessoa)
