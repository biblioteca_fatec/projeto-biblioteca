# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from apps.nucleo.models import Endereco


class EnderecoAdmin(admin.ModelAdmin):
    list_display = ['rua', 'numero', 'bairro', 'complemento', 'cidade', 'estado', 'cep']
    list_filter = ['state']
    fieldsets = (
        (None, {
            'fields': ('contato', 'telefone', 'rua', 'numero', 'bairro', 'complemento', 'cidade', 'estado', 'cep', 'localizacao')
        }),
        ('Publicação', {
            'fields': ('publicar', 'publicar_telefone')
        }),
    )