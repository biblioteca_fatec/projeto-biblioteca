from .pessoa import (
    PessoaAdmin,
    PessoaFisicaAdmin,
    PessoaEnderecoInline,
    PessoaImagemInline,
)