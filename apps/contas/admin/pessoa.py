from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.contrib.auth.forms import AdminPasswordChangeForm
from django.utils.translation import gettext as _
from ..models import PessoaEndereco, PessoaImagem, PessoaFisica


class PessoaEnderecoInline(admin.StackedInline):
    model = PessoaEndereco
    fieldsets = (
        (None, {'fields': ('contato', 'telefone', 'address_type',)}),
        (None, {'fields': ('rua', 'numero')}),
        (None, {'fields': ('complemento', 'bairro')}),
        (None, {'fields': ('cidade', 'estado', 'cep', 'localizacao')}),
        (None, {'fields': ('publicar', 'publicar_telefone')}),
    )
    extra = 1
    max_num = 1


class PessoaImagemInline(admin.TabularInline):
    model = PessoaImagem


class PessoaAdmin(UserAdmin):
    change_password_form = AdminPasswordChangeForm

    list_display = ['email', 'is_active', 'date_joined', 'last_login']

    fieldsets = (
        (None, {'fields': ('email',)}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'about', 'avatar')}),
        (_('Permissions'), {'fields': ('is_active',)}),
    )

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('first_name', 'last_name', 'email', ),
        }),
    )

    inlines = UserAdmin.inlines + [PessoaEnderecoInline, PessoaImagemInline]


class PessoaFisicaAdmin(admin.ModelAdmin):
    pass




class MyUserAdmin(UserAdmin):
    list = ('username', 'email', 'first_name', 'last_name', 'is_staff', '_impersonate')

    def _impersonate(self, obj):
        return format_html('<a href="{}">Impersonate</a>', obj.get_impersonate_url())


admin.site.register(PessoaFisica, PessoaFisicaAdmin)
# admin.site.register(PessoaJuridica, PessoaJuridicaAdmin)



