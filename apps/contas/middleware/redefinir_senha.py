from threading import local
from django.urls import resolve, reverse
from django.http import HttpResponseRedirect
_thread_locals = local()


class RedefinirSenhaMiddleware(object):
    def process_request(self, request):
        _thread_locals.request = request
        if request.user.is_authenticated():
            if hasattr(request.user, 'pessoa'):
                if request.user.person.reset_password:
                    if resolve(request.path).url_name != 'account_password_update':
                        return HttpResponseRedirect(reverse('account_password_update'))
