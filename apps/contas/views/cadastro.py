from django.views.generic import FormView
from django.urls.base import reverse_lazy
from ..forms import PessoaFisicaForm


class CadastroView(FormView):
    form_class = PessoaFisicaForm
    template_name = 'contas/cadastro.html'
    success_url = reverse_lazy('cadastro_realizado')

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)
