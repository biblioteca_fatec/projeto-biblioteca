from django.views import generic
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.http import JsonResponse
from django.contrib.auth.mixins import LoginRequiredMixin
from apps.contas.models import PessoaFisica
from ..forms import ResetarSenhaForm, RecuperarSenhaForm, TrocarSenhaForm


class ResetarSenhaView(generic.FormView):
    form_class = ResetarSenhaForm
    template_name = 'contas/include/resetar_senha.html'
    usuario = None

    def dispatch(self, request, *args, **kwargs):
        self.pessoa = PessoaFisica.objects.filter(id=kwargs.get('pessoa_id')).first()
        return super().dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['pessoa'] = self.pessoa
        return kwargs

    def form_valid(self, form):
        form.resetar_senha()
        return JsonResponse({'ok': True}, safe=True)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['pessoa'] = self.pessoa
        return context


class RecuperarSenhaView(SuccessMessageMixin, generic.FormView):
    template_name = 'contas/redefinir_senha.html'
    form_class = RecuperarSenhaForm
    success_url = reverse_lazy('login')

    def form_valid(self, form):
        form.redefinir_senha()
        return super().form_valid(form)


class TrocarSenhaView(LoginRequiredMixin, generic.FormView):
    template_name = 'contas/trocar_senha.html'
    form_class = TrocarSenhaForm
    success_url = reverse_lazy('index')

    def get_form_kwargs(self):
        k = super().get_form_kwargs()
        k.update({
            'user': self.request.user
        })
        return k

    def form_valid(self, form):
        form.trocar_senha()
        return super().form_valid(form)
