from .login import LoginView, LogoutView
from .cadastro import CadastroView
from .senha import ResetarSenhaView, RecuperarSenhaView, TrocarSenhaView
