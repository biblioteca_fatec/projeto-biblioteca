from django.views import View
from django.shortcuts import render
from django.urls import reverse
from django.contrib.auth import logout as django_logout, login as django_login
from django.http.response import HttpResponseRedirect
from django.forms.utils import ErrorList

from ..forms import (
    LoginForm,
    PessoaFisicaForm
)


class LogoutView(View):
    def get(self, request):
        django_logout(request)
        return HttpResponseRedirect(reverse('login'))


class LoginView(View):
    template_name = 'contas/login.html'

    def post(self, request, *args, **kwargs):
        form = LoginForm(request.POST)
        if not request.session.test_cookie_worked():  # teste cookie
            form.errors["usuario"] = ErrorList(["Por favor habilite os cookies."])
        elif form.is_valid():
            request.session.delete_test_cookie()           
            django_login(request, form.user)
            retorno = 'next' in request.GET and request.GET['next'] or reverse('home')
            request.session.set_expiry(0)
            return HttpResponseRedirect(retorno)
        form_cadastro = PessoaFisicaForm()
        return render(request, self.template_name, locals())

    def get(self, request, *args, **kwargs):
        form = LoginForm()
        form_cadastro = PessoaFisicaForm()
        request.session.set_test_cookie()
        return render(request, self.template_name, locals())

