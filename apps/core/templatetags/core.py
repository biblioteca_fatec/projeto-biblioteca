from django import template
from django.contrib.humanize.templatetags.humanize import intcomma
import re

register = template.Library()


@register.inclusion_tag('core/templatetags/include-field.html')
def include_field(form_field_instance):
    return {'field': form_field_instance}


@register.filter
def money(value):
    if value:
        value = round(float(value), 2)
        return "R$ %s%s" % (intcomma(int(value), use_l10n=True), ("%0.2f" % value)[-3:])
    else:
        return 'R$ 0,00'


@register.filter
def percent(value):
    if value:
        value = round(float(value), 2)
        return "%s %%" % int(value).__str__()
    else:
        return '0 %%'


@register.simple_tag(takes_context=True)
def get_perfil_usuario(context):
    request = context.get('request')
    try:
        return request.user.pessoa
    except:
        pass
    return request.user


@register.simple_tag
def get_perfil():
    return Perfil.objects.get_current()


@register.simple_tag
def easy_static(path):
    return static('%s' % (path))


@register.simple_tag
def get_label_css_class(status):
    status = re.sub('[^A-Za-z0-9]+', '', str(status)) #adicionei str() p/ status, estava dando erro no dashboard do crm!
    return 'label-%s' % str(status).lower()


@register.filter(name='currency')
def currency(value):
    return "R$ {}".format(value)


@register.filter(name='sigla')
def sigla(value):
    sigla = ""
    if value:
        str_list = value.split()
        if len(str_list) > 1:
            for str in str_list:
                sigla = "{}{}".format(sigla, str[0])
        else:
            sigla = "{}{}".format(str_list[0][0], str_list[0][1])
    return sigla

