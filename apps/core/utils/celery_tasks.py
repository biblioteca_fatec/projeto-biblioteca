from celery import task
from celery.five import monotonic
from contextlib import contextmanager
from django.core.cache import cache
from hashlib import md5


LOCK_EXPIRE = 60 * 10  # Lock expires in 10 minutes

@contextmanager
def memcache_lock(lock_id, oid, lock_expire=120):
    """
    timeout em segundos
    """
    timeout_at = monotonic() + lock_expire - 3
    # cache.add fails if the key already exists
    status = cache.add(lock_id, oid, lock_expire)
    try:
        yield status
    finally:
        # memcache delete is very slow, but we have to use it to take
        # advantage of using add() for atomic locking
        if monotonic() < timeout_at:
            # don't release the lock if we exceeded the timeout
            # to lessen the chance of releasing an expired lock
            # owned by someone else.
            cache.delete(lock_id)

def get_lock_id(task_intance, target_instance):
    target_instance_hexdigest = md5(target_instance).hexdigest()
    lock_id = '{0}-lock-{1}'.format(task_intance.name, target_instance_hexdigest)
    return lock_id   