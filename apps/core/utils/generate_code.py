import hashlib
import time


def generate_code(len=40):
    hash = hashlib.sha1()
    hash.update(time.time().__str__().encode('utf-8'))
    return hash.hexdigest()[:len]
