import re

def somente_nro(str):
    return re.sub("[^\d]+", "",str)


def DV_maker(v):
    if v >= 2:
        return 11 - v
    return 0

def cnpj_is_valid(value):
    if not value.isdigit():
        value = re.sub("[-/\.]", "", value)
    try:
        int(value)
    except ValueError:
        return False
    if len(value) != 14:
        return False
    orig_dv = value[-2:]

    new_1dv = sum([i * int(value[idx]) for idx, i in enumerate(range(5, 1, -1) + range(9, 1, -1))])
    new_1dv = DV_maker(new_1dv % 11)
    value = value[:-2] + str(new_1dv) + value[-1]
    new_2dv = sum([i * int(value[idx]) for idx, i in enumerate(range(6, 1, -1) + range(9, 1, -1))])
    new_2dv = DV_maker(new_2dv % 11)
    value = value[:-1] + str(new_2dv)
    return value[-2:] == orig_dv

def cpf_is_valid(value):
    if not value.isdigit():
        value = re.sub("[-\.]", "", value)
    try:
        int(value)
    except ValueError:
        return False
    if len(value) != 11:
        return False
    orig_dv = value[-2:]

    new_1dv = sum([i * int(value[idx]) for idx, i in enumerate(range(10, 1, -1))])
    new_1dv = DV_maker(new_1dv % 11)
    value = value[:-2] + str(new_1dv) + value[-1]
    new_2dv = sum([i * int(value[idx]) for idx, i in enumerate(range(11, 1, -1))])
    new_2dv = DV_maker(new_2dv % 11)
    value = value[:-1] + str(new_2dv)
    return value[-2:] == orig_dv


def formata_cnpj(cnpj):
    return "%s.%s.%s/%s-%s" % (cnpj[0:2], cnpj[2:5], cnpj[5:8], cnpj[8:12], cnpj[12:14])


def formata_cpf(cpf):
    try:
        value = re.sub("[-\.]", "", cpf)
        return "%s.%s.%s-%s" % ( value[0:3], value[3:6], value[6:9], value[9:11] )
    except:
        return cpf
        pass


def formata_cep(cep):
    return "%s.%s-%s" % ( cep[0:2], cep[2:5], cep[5:8] )


def format_number(number, precision=0, group_sep='.', decimal_sep=','):
    number = ('%.*f' % (max(0, precision), number)).split('.')
    integer_part = number[0]
    if integer_part[0] == '-':
        sign = integer_part[0]
        integer_part = integer_part[1:]
    else:
        sign = ''
    if len(number) == 2:
        decimal_part = decimal_sep + number[1]
    else:
        decimal_part = ''

    integer_part = list(integer_part)
    c = len(integer_part)

    while c > 3:
        c -= 3
        integer_part.insert(c, group_sep)

    return sign + ''.join(integer_part) + decimal_part
