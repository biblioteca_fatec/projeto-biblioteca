from __future__ import unicode_literals


def getattrs_as_dict(instance, field_list):
    """
    retorna um dicionario com os itens definidos em field_list, que pode ser uma lista ou tupla
    """
    data = {}
    for field in field_list:
        data.update({field: getattr(instance, field)})

    return data
