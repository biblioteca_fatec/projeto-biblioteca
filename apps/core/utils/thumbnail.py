from django.templatetags.static import static
from sorl.thumbnail import get_thumbnail


class ThumbnailMixin(object):
    thumb_field = 'imagem'
    thumb_geometry = '200x200'
    thumb_default = 'images/default/imagem.png'
    thumb_format = 'PNG'
    thumb_crop = 'center'

    @property
    def thumb(self):
        img_attr = getattr(self, self.thumb_field, None)
        if not img_attr:
            if not self.thumb_default:
                return None
            return static(self.thumb_default)
        return get_thumbnail(img_attr, self.thumb_geometry, format=self.thumb_format, crop=self.thumb_crop).url


class BannerMixin(object):
    banner_field = 'banner'
    banner_geometry = '1922x393'
    banner_format = 'PNG'
    banner_crop = 'center'
    banner_default = 'images/default/banner.png'

    @property
    def banner(self):
        img_attr = getattr(self, self.banner_field, None)
        if not img_attr:
            if not self.banner_default:
                return None
            return static(self.banner_default)
        return get_thumbnail(img_attr, self.banner_geometry, format=self.banner_format, crop=self.banner_crop).url
