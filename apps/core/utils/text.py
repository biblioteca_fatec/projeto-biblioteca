from __future__ import unicode_literals

import re


def only_numbers(val):
    return re.sub('[^0-9]', '', val)
