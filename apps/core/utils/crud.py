
def get_crud_urls_for(view_list=[]):
    """
    Pega as as URLS para os tipos passado numa lista
    """
    ret = []
    for view in view_list:
        ret = ret + view().get_urls()
    return ret
