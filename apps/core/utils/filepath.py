from __future__ import unicode_literals

import unicodedata
import re
from django.utils.safestring import mark_safe
from django.utils import six


def rename_non_ascii(value):
    """
    Normalizes string, converts to lowercase, removes non-alpha characters,
    and converts spaces to hyphens.
    """
    value = six.text_type(re.sub('[^\w\s\.-]', '', value).strip().lower())
    value = re.sub('[-\s]+', '-', value)
    value = unicodedata.normalize(
        'NFKD', six.text_type(value)).encode('ascii', 'ignore')
    return mark_safe(value)


def get_upload_path(arg1, arg2):
    return '%s/%s' % (arg1.__class__.__name__, rename_non_ascii(arg2))
