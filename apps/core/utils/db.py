from django.template.defaultfilters import slugify


def slug_sender_pre_save(sender, instance, **kwargs):
    """
    Tenta retornar o valor da propriedade slug_with_this do objeto ou o campo name.
    """
    slug = getattr(instance,'slug_with_this') if hasattr(instance, 'slug_with_this') else getattr(instance, 'name', None)

    slug = slugify(slug)

    new_slug = slug

    i = 0
    while sender.objects.filter(slug=new_slug).exclude(id=instance.id).count() > 0:
        i += 1
        new_slug = '%s-%d' % (slug, i)
    instance.slug = new_slug.lower()
