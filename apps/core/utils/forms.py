import re
from django.forms.utils import ErrorList
from django.utils.translation import gettext as _


cpf_digits_re = re.compile(r'^(\d{3})\.(\d{3})\.(\d{3})-(\d{2})$')


def dv_maker(v):
    if v >= 2:
        return 11 - v
    return 0


class FormValidationsMixin(object):
    
    def validate_required(self, field_names=[]):
        c = self.cleaned_data
        msg = None
        for field_name in field_names:
            if not c.get(field_name, ''):
                msg = self.fields[field_name].error_messages['required']
                self._errors[field_name] = ErrorList([msg])


    def validate_duplicated(self, field_name, __model__):
        value = c = self.cleaned_data.get(field_name, None)
        if value is not None:
            if __model__.objects.filter(**{field_name: value}).exists():
                msg = _('Entradas inválidas. Valor já registrado em nosso banco de dados.')
                self._errors[field_name] = ErrorList([msg])


                

    def validate_not_equal(self, field_names=[]):
        """
        tupla de campos para checagem
        [(field_name, field_name_2), (field_name, field_name_2)]
        """
        c = self.cleaned_data
        for field_name, field_name_2 in field_names:
            if c.get(field_name, 'field_name') != c.get(field_name_2, 'field_name_2'):
                msg = _('Entradas inválidas')
                self._errors[field_name_2] = ErrorList([msg])

    def validate_cpf(self, field_name):
        """Value can be either a string in the format XXX.XXX.XXX-XX or an 11-digit number."""
        
        value = c = self.cleaned_data.get(field_name, None)
        
        if value is None:
            return value

        orig_value = value[:]
        error_msg = None
        if not value.isdigit():
            cpf = cpf_digits_re.search(value)
            if cpf:
                value = ''.join(cpf.groups())
            else:
                error_msg = _('Entrada inválida')
                self._errors[field_name] = ErrorList([error_msg])
                return orig_value

        if len(value) != 11:
            error_msg = _('Utilize CPF com 11 dígitos')
            self._errors[field_name] = ErrorList([error_msg])
            return orig_value
        orig_dv = value[-2:]

        new_1dv = sum([i * int(value[idx])
                      for idx, i in enumerate(range(10, 1, -1))])
        new_1dv = dv_maker(new_1dv % 11)
        value = value[:-2] + str(new_1dv) + value[-1]
        new_2dv = sum([i * int(value[idx])
                      for idx, i in enumerate(range(11, 1, -1))])
        new_2dv = dv_maker(new_2dv % 11)
        value = value[:-1] + str(new_2dv)
        if value[-2:] != orig_dv:
            error_msg = _('Entrada inválida')
            self._errors[field_name] = ErrorList([error_msg])
            return orig_value
        if value.count(value[0]) == 11:
            error_msg = _('Utilize CPF com 11 dígitos')
            self._errors[field_name] = ErrorList([error_msg])
            return orig_value
        
        return orig_value
