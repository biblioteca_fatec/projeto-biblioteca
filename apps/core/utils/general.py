
def pegar_mes_para_int(mes):
    mes = int(mes)
    meses = ('Janeiro', 'Fevereiro', u'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro')
    return meses[mes-1]


def split_name(instance,field):
    """
    seta first and last name do authuser
    """
    first = ''
    last = ''
    try:
        s = field.split()
        first = s[0]
        if len(s) > 1:
            last = s[len(s) - 1]
    except:
        pass
    instance.first_name = first
    instance.last_name = last