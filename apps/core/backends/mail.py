from django.contrib.auth.backends import ModelBackend
from django.contrib.auth import get_user_model


class AuthUserByEmailBackend(ModelBackend):
    def authenticate(self, username=None, password=None, **kwargs):
        user = None
        try:
            user_model = get_user_model()
            user = user_model.objects.get(email__iexact=username)
        except Exception:
            return None

        if user.check_password(password):
            return user
        else:
            return None