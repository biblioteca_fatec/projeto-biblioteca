from __future__ import unicode_literals

from django.apps import AppConfig

VERSION = "0.1"
__version__ = VERSION


class Config(AppConfig):
    name = 'apps.core'
    verbose_name = 'Conjunto de Classes e Métodos'

default_app_config = 'apps.core.Config'
