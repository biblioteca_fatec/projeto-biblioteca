import json
from django.views import generic
from django.http import HttpResponseRedirect, HttpResponse
from django.core.serializers import serialize
from django.core.exceptions import ImproperlyConfigured


class ModelFormGenericView(generic.FormView, generic.edit.ModelFormMixin):
    """
    Suporte padroes para views baseada em model form
    """
    template_name = None
    form_class = None
    success_url = None
    redirect_url = None
    redirect_field_name = 'redirect_to'

    def dispatch(self, request, *args, **kwargs):
        if request.GET.has_key(self.redirect_field_name):
            self.redirect_url = request.GET.get(self.redirect_field_name)
            self.success_url = self.redirect_url
        else:
            self.redirect_url = self.success_url

        return super(ModelFormGenericView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        kwargs.update({self.redirect_field_name: self.redirect_url})
        return super(ModelFormGenericView, self).get_context_data(**kwargs)

    def form_valid(self, form):
        return HttpResponseRedirect(self.get_success_url())


class JSONResponseMixin(object):

    def render_to_json_response(self, context, **kwargs):
        kwargs.update({'content_type': 'application/json'})
        return HttpResponse(self.get_data(context), **kwargs)

    def get_data(self, context):
        if context.get('object_str', None):
            return context.get('object_str')
        elif context.get('object_dict', None):
            return json.dumps(
                context.get('object_dict')
            )
        elif context.get('object', None):
            return serialize(
                'json',
                context.get('object')
            )

        raise ImproperlyConfigured("You must inform an object_str(string), object_dict(dict) or object(queryset)")
