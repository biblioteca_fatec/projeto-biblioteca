from django.urls import path

from apps.email_sender import views

urlpatterns = [
    path('preview/', views.MessagePreView.as_view(), name='message_preview'),
]
