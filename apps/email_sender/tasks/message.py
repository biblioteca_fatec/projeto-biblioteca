from __future__ import unicode_literals

from celery.decorators import task
from celery.utils.log import get_task_logger

from django.core.management import call_command

logger = get_task_logger(__name__)

@task
def send():
    raise Exception('do me!')