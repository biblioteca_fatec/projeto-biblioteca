from __future__ import unicode_literals

from django.db import models


class MessageTag(models.Model):
    class Meta:
        verbose_name = "Tag de Mensagem"
        verbose_name_plural = "Tags de Mensagem"
   
    tag = models.CharField(
        'Tag', max_length=300, unique=True
    )

    def __str__(self):
        return self.tag

    @classmethod
    def get_or_create(cls, tag):
        o,f = cls.objects.get_or_create(tag=tag)
        return o