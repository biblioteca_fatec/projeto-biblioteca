from __future__ import unicode_literals

from django.db import models
from django.conf import settings
from django.core.mail import EmailMultiAlternatives, get_connection
from django.core.mail.backends.smtp import EmailBackend

from django.template import loader
from django.template.context import Context

from django.utils.encoding import smart_str as smart_unicode
from django.utils.translation import ugettext_lazy as _
from smtplib import SMTP

import threading
import time


class SMTPServerManager(models.Manager):
    def get_current(self):
        try:
            servidor = self.all()[:1]
            if servidor.count() == 0:
                servidor = SMTPServer()
                servidor.save()
            else:
                servidor = servidor[0]
            return servidor
        except Exception as e:
            raise Exception(_('Servidor SMTP não configurado. Por favor, cadastre um na Área Admin'))


_SENDING_ALTERNATIVE_EMAIL = False


class SMTPServer(models.Model):
    """
    Main Mail Server
    """
    class Meta:
        verbose_name = _("Meu Servidor")
        verbose_name_plural = _("Meus Servidores")

    objects = SMTPServerManager()

    alias = models.CharField(_("Apelido"),
                                max_length=255, default="Gmail",
                                help_text="Uma descrição para o servidor")

    host = models.CharField(_("Host SMTP"),
                                max_length=255, unique=True, default="smtp.gmail.com",
                                help_text="Informação do servidor smtp, exemplo: smtp.gmail.com")
    
    port = models.IntegerField(_("Porta"),
                                default=587,
                                help_text="Informar a porta do servidor smtp.")
    
    username = models.CharField(_("Nome de Usuário"),
                                max_length=128, blank=True, default="websites@os3ti.com",
                                help_text="Usuário do servidor smtp. deixar em branco caso o smtp seja público.")
    
    password = models.CharField(_("Senha"),
                                max_length=128, blank=True, default="H>a6D54R",
                                help_text="Senha do servidor smtp. deixar em branco caso o smtp seja público.")

    use_tls = models.BooleanField(_('TLS'),
                                    default=True,
                                    help_text='Informe se o servidor usa TLS')

    use_ssl = models.BooleanField(_('SSL'),
                                  default=False,
                                  help_text='Informe se o servidor usa SSL.')

    from_email = models.CharField(_("Email de envio"),
                                    max_length=255, default="FATEC ARARAQUARA<websites@fatec.com>",
                                    help_text='Email usado para enviar, geralmente é o mesmo conteúdo do campo usuário.')

    reply_to = models.CharField(_("Responder para"),
                                    max_length=255, default="OS3<websites@fatec.com>",
                                    help_text='O endereço configurado será colocado automaticamente no campo resposta.')

    timeout = models.IntegerField(_("Timeout"),
                                    blank=True, null=True)

    ssl_keyfile = models.FileField(_("SSL Key"),
                                    upload_to='mail-server/keys/', blank=True, null=True)

    ssl_certfile = models.FileField(_("SSL Cert"),
                                    upload_to='mail-server/cert/', blank=True, null=True)

    def __str__(self):
        return self.alias

    def check_connection(self):
        """
        Verifica se é possivel conectar no servidor smtp.
        """
        try:
            smtp = SMTP(self.host, int(self.port))
            if self.use_tls:
                smtp.starttls()
            if self.username or self.password:
                smtp.login(self.username, self.password)
            smtp.quit()
        except Exception:
            return False
        return True
    check_connection.short_description = u'Testar conexão'

    def send_mail(self, subject, to, **kwargs):
        """
            envia email
            kwargs
                - template manda email usando o template do django
                    - opcional context
                - messagem -> usa string comum
                - reply_to -> email de resposta
                - attach_files -> lista de documentos anexos

        """
        global _SENDING_ALTERNATIVE_EMAIL
        try:
            while _SENDING_ALTERNATIVE_EMAIL:
                time.sleep(1)
            _SENDING_ALTERNATIVE_EMAIL = True
            template = kwargs.get('template', None)
            mensagem = kwargs.get('mensagem', None)
            attach_files = kwargs.get('attach_files', [])
            mime_image_list = kwargs.get('mime_image_list', [])
            mail_format = kwargs.get('mail_format', 'text/html')
            context = kwargs.get('context', Context())
            reply_to = kwargs.get('reply_to', self.from_email)
            if type(to) is not list:
                to = [to]
            if template is None and mensagem is None:
                raise Exception(
                    u'Você precisa informar uma mensagem ou um template como parametro')

            subject = smart_unicode(subject)
            texto_puro = u'Esse email precisa ser visualizado por um cliente de email que aceite html'
            
            if template:
                html = loader.get_template(template).render(context)
            else:
                html = mensagem
                

            # verifica o tipo de backend
            if settings.EMAIL_BACKEND.lower() == 'django.core.mail.backends.smtp.emailbackend':
                _connection = EmailBackend(
                    self.host, self.port, self.username, self.password, self.use_ssl)
            else:
                _connection = get_connection()

            msg = EmailMultiAlternatives(subject,
                                         texto_puro,
                                         from_email=self.from_email,
                                         to=to,
                                         headers={'Reply-To': reply_to},
                                         connection=_connection)

            msg.attach_alternative(html, mail_format)

            if attach_files:
                for file_path in attach_files:
                    msg.attach_file(file_path)
            for a in mime_image_list:
                msg.attach(a)
            msg.send()

        except Exception as e:
            raise e
        finally:
            _SENDING_ALTERNATIVE_EMAIL = False

    def send_mail_thread(self, subject, to, **kwargs):
        """
        envio de email usando thread
        """
        t = threading.Thread(target=self.send_mail,
                             args=[subject, to], kwargs=kwargs)
        t.start()
