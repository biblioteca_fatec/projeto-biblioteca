from __future__ import unicode_literals

from django.db import models
from django.utils import timezone

from apps.email_sender.util import send_html_message


class Message(models.Model):
    class Meta:
        verbose_name = 'Mensagem'
        verbose_name_plural = 'Mensagens'

    subject = models.CharField(
        "Título",
        max_length=300
    )

    html_content = models.TextField(
        "HTML Format"
    )

    text_content = models.TextField(
        "Text/Plain",
        default=''
    )

    email_from = models.ForeignKey(
        'email_sender.EmailAddress',
        related_name='email_from_messages',
        verbose_name='Enviado por',
        on_delete=models.PROTECT
    )

    reply_to = models.ForeignKey(
        'email_sender.EmailAddress',
        related_name='reply_to_messages',
        verbose_name='Responder para', 
        on_delete=models.PROTECT, 
        null=True, blank=True
    )

    to = models.ManyToManyField(
        'email_sender.EmailAddress',
        related_name='to_messages',
        verbose_name='Para'
    )

    cc = models.ManyToManyField(
        'email_sender.EmailAddress',
        related_name='cc_messages',
        verbose_name='Com cópia para',
        blank=True
    )

    bcc = models.ManyToManyField(
        'email_sender.EmailAddress',
        related_name='bcc_messages',
        verbose_name='Com cópia oculta para',
        blank=True
    )


    tags = models.ManyToManyField(
        'email_sender.MessageTag',
        related_name='message_tags',
        verbose_name='Tags da Mensagem',
        blank=True
    )

    CREATED = 0
    SENT = 1
    RETRY = 2
    ERROR_ON_SENDING = 3

    STATUS = [
        (CREATED, 'Criado'),
        (SENT, 'Enviado'),
        (ERROR_ON_SENDING, 'Erro ao Enviar'),
    ]

    status = models.SmallIntegerField(
        'Estado',
        choices=STATUS,
        default=CREATED
    )

    created_date = models.DateTimeField(
        'Criado em',
        auto_now=True
    )

    last_attempt_date = models.DateTimeField(
        'Ultima tentativa em',
        auto_now_add=True
    )

    sent_date = models.DateTimeField(
        'Enviado em',
        null=True
    )

    server = models.ForeignKey(
        'email_sender.SMTPServer',
        related_name='server_messages',
        on_delete=models.PROTECT
    )
    
    server_response = models.TextField(
        'Resposta do Servidor',
        default='',
        blank=True
    )

    batch_id = models.CharField("Lote", max_length=300, default="")


    def __str__(self):
        return self.subject

    @classmethod
    def create(cls, **kwargs):
        obj = cls.objects.create(**kwargs)
        return obj

    @classmethod
    def find(cls, message_id):
        return cls.objects.filter(id=message_id).first()
    
    # adiciona destionarios
    def add_to(self, email_addresses):
        """        
        Arguments:
            email_addresses -- Array de Email Address
        """
        for e in email_addresses:
            self.to.add(e)

    def add_cc(self, email_addresses):
        """        
        Arguments:
            email_addresses -- Array de Email Address
        """
        for e in email_addresses:
            self.cc.add(e)

    def add_bcc(self, email_addresses):
        """        
        Arguments:
            email_addresses -- Array de Email Address
        """
        for e in email_addresses:
            self.bcc.add(e)

    # tags
    def add_tags(self, tags):
        """        
        Arguments:
        email_addresses -- Array de Email Address
        """
        for e in tags:
            self.tags.add(e)

    # anexos
    def add_attachments(self, uploaded_files):
        """        
        Arguments:
            uploaded_files {[type]} -- Array InMemoryUploadedFiles
        """

        for f in uploaded_files:
            self.message_attachments.create(attachment=f)

    # retorna arrays de enderecos apenas
    def get_to(self):
        return [e.email for e in self.to.all()]

    def get_cc(self):
        return [e.email for e in self.cc.all()]

    def get_bcc(self):
        return [e.email for e in self.bcc.all()]

    def get_from_email(self):
        return self.email_from.email
    
    def get_reply_to(self):
        return [self.reply_to.email]

    def get_attachments(self):
        return [ a.get_path() for a in self.message_attachments.all() ]

    # retorna array de tags
    def get_tags(self):
        return [e.tag for e in self.tags.all()]
        
    # envia e-mail de fato
    def send(self):
        try:
            result = send_html_message(
                self.subject, self.html_content, self.get_to(),
                plain_text_body=self.text_content,
                from_email=self.get_from_email(), reply_to=self.get_reply_to(),
                cc=self.get_cc(), bcc=self.get_bcc(),
                files=self.get_attachments()
            )
            self.status = self.SENT
            self.server_response = ""
        except Exception as ex:
            self.status = self.ERROR_ON_SENDING
            self.server_response = ex
        
        self.sent_date = timezone.now()
        
        self.save()

