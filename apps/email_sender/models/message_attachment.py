from __future__ import unicode_literals

from django.db import models


class MessageAttachment(models.Model):
    class Meta:
        verbose_name = "Anexo"
        verbose_name_plural = "Anexos"
    
    message = models.ForeignKey(
        'email_sender.Message',
        related_name='message_attachments',
        verbose_name='Mensagem',
        on_delete=models.PROTECT
    )
    attachment = models.FileField("Anexo", upload_to="message_attachments")
    title = models.CharField("Descrição", max_length=300, default="", blank=True)

    def __str__(self):
        return "Anexo {}".format(self.id)

    def get_path(self):
        return self.attachment.path
