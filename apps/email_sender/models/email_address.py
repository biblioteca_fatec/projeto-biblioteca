from __future__ import unicode_literals

from django.db import models


class EmailAddress(models.Model):
    class Meta:
        verbose_name = 'Endereço de E-mail'
        verbose_name_plural = 'Endereços de E-mail'

    email = models.CharField(
        'E-mail',
        max_length=300,
        unique=True
    )

    def __str__(self):
        return self.email

    @classmethod
    def get_or_create(cls, email):
        o, f = cls.objects.get_or_create(email=email)
        return o
