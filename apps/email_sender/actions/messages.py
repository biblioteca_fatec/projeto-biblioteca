from __future__ import unicode_literals

from django.db import transaction

from ..models import (
    Message, EmailAddress, SMTPServer, MessageTag
)

@transaction.atomic()
def create_message(subject, html_content, to,
                    text_content='', email_from='',
                    reply_to='', cc=[], bcc=[],
                    tags=[], attachments=[], batch_id=''):
    """[summary]

    Arguments:
        subject {[type]} -- Assunto
        html_content {[type]} -- Formato HTML
        to {list} -- E-mails para envio Array

    Keyword Arguments:
        text_content {str} -- Formato texto plano (default: {''})
        email_from {str} -- E-mail de envio (default: {''})
        reply_to {str} -- E-mail de resposta (default: {''})
        cc {list} -- E-mails em copia Array
        bcc {list} -- E-mails em copia oculta Array
        tags -- MessageTag Array
        attachments {list} -- InMemoryUploadedFile Array
        batch_id {str} - Id do Lote
    """

    # smtp server atual
    server = SMTPServer.objects.get_current()
    email_from = email_from if email_from else server.from_email
    reply_to = reply_to if reply_to else server.from_email

    # metodo da classe que cria um endereco de email
    create_email = EmailAddress.get_or_create
    create_tag = MessageTag.get_or_create

    # cria mensagen
    message = Message.create(
        subject=subject, 
        html_content=html_content,
        text_content=text_content,
        email_from=create_email(email_from),
        reply_to=create_email(reply_to),
        server=server,
        batch_id=batch_id
    )
    
    # adiciona destinatarios
    message.add_to([ create_email(e) for e in to ])
    message.add_cc([ create_email(e) for e in cc ])
    message.add_bcc([ create_email(e) for e in bcc ])
    
    #adicion tags
    message.add_tags( [ create_tag(t) for t in tags ] )

    # adiciona anexos
    message.add_attachments(attachments)

    return message


@transaction.atomic()
def send_message(message_id):
    """
    Envia a mensagem    
    Arguments:
        message_id {[type]} -- Id da Mensagem
    
    Returns:
        [type] -- Message
    """
    message = find_message(message_id)
    message.send()
    return message


def find_message(message_id):
    """
    Encontra uma mensagem    
    Arguments:
        message_id {[type]} -- Id da Mensagem
    
    Returns:
        [type] -- Message
    """
    return Message.find(message_id)

