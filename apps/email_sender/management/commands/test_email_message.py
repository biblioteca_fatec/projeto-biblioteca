from django.core.management.base import BaseCommand
from django.utils import timezone

from apps.email_sender.actions import (
    create_message, send_message
)

class Command(BaseCommand):
    help = 'Teste de E-mail'

    def handle(self, *args, **kwargs):
        # message = create_message(
        #     'Assunto',
        #     '<b>HTML FORMAT<b>',
        #     ['breno@os3ti.com'],
        #     text_content='PLAIN FORMAT',
        #     email_from='OS3 Teste Mudou<informe@os3ti.com>',
        #     reply_to='OS3 Nao Informa<informe@os3ti.com>',
        #     cc=['suporte@os3ti.com'],
        #     bcc=['silva.breno@gmail.com'],
        # )

        # message.send()
        send_message(16)
        self.stdout.write("teste finalizado")