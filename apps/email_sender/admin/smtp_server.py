from __future__ import unicode_literals

from django.contrib import admin
from django.forms.models import ModelForm
from django import forms

from ..models import SMTPServer


class SMTPServerForm(ModelForm):
    class Meta:
        model = SMTPServer
        exclude = ('timeout', 'ssl_keyfile', 'ssl_certfile')

    host = forms.CharField(label=u"Servidor SMTP", max_length=255,
                           widget=forms.TextInput(attrs={'data-ignoreuppercase': 'True'}))
    username = forms.CharField(label=u"Usuário", max_length=128, required=False,
                               widget=forms.TextInput(attrs={'data-ignoreuppercase': 'True'}))
    password = forms.CharField(label=u"Senha", max_length=128, required=False,
                               widget=forms.TextInput(attrs={'data-ignoreuppercase': 'True'}))
    from_email = forms.CharField(label=u"Email de envio", max_length=255, widget=forms.TextInput(
        attrs={'data-ignoreuppercase': 'True'}))

@admin.register(SMTPServer)
class SMTPServerAdmin(admin.ModelAdmin):
    # form = SMTPServerForm
    list_display = ['alias', 'host', 'port',
                    'username', 'use_tls', 'use_ssl', 'from_email']
    list_filter = ['use_tls', 'use_ssl']
    search_fields = ['alias', 'host', 'username']

    fieldsets = (
        (None, {'fields': ('alias',)}),
        ('Configurações', {'fields': ('host', 'port', 'username',
                                      'password', 'from_email', 'use_tls', 'use_ssl'), }),
    )

    actions = ['check_connections']

    def check_connections(self, request, queryset):
        """
            Checa conexao com os servidores smtp
        """
        message = '%s - %s'
        for server in queryset:
            if server.check_connection():
                status = u"%s" % 'conectado com sucesso.'
            else:
                status = "%s" % ' impossível conectar.'
            self.message_user(request, message % (server, status))
    check_connections.short_description = 'Testar conexão'

