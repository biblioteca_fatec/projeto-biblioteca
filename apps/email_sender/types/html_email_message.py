from django.core.mail import EmailMultiAlternatives
from django.template import loader
from django.contrib.sites.models import Site


class HTMLEmailMessage(EmailMultiAlternatives):
    """
    Mensagens HTML
    Aceita Template
    """
    TEXT_HTML = 'text/html'

    def __init__(
            self, subject, body, to,
            template=None, context=None, plain_text_body=None,
            from_email=None, cc=None, bcc=None, reply_to=None, connection=None,
            files=None, inline_images=None, attachments=None, headers=None, alternatives=None):

        # formato texto plano
        text_body = plain_text_body if plain_text_body else body

        # formato html
        if context is None:
            context = {
                'text_body': text_body,

            }
        else:
            context.update({'text_body': text_body})

        html_body = loader.get_template(template).render(context)

        #chama super
        super(HTMLEmailMessage, self).__init__(
            subject=str(subject),
            body=str(text_body),
            from_email=from_email,
            to=to,
            bcc=bcc,
            connection=connection,
            attachments=attachments,
            headers=headers,
            alternatives=alternatives,
            cc=cc,
            reply_to=reply_to
        )

        self.attach_alternative(str(html_body), self.TEXT_HTML)

        if files:
            for fp in files:
                self.attach_file(fp)

        if inline_images:
            for fb in inline_images:
                self.attach(fb)
