from __future__ import unicode_literals

from django.views.generic import TemplateView
from django.shortcuts import get_object_or_404
from django.contrib.sites.models import Site

from ..models import Message

class MessagePreView(AdminOnlyMixin, TemplateView):
    template_name = "email_sender/message/content.html"
    subject_template_name = "email_sender/message/subject.txt"
    
    def get_context_data(self, *args, **kwargs):
        message = {} #TODO message model
        
        if notification is not None:
            kwargs.update(
                {
                    'notification': notification,
                    'site': Site.objects.get_current(),
                    'http': 'http://%s' % Site.objects.get_current().domain
                }
            )
        return kwargs
