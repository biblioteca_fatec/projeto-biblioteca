from __future__ import unicode_literals

from django.apps import AppConfig

VERSION = "0.1"
__version__ = VERSION


class Config(AppConfig):
    name = 'apps.email_sender'
    verbose_name = 'Servidores de E-mail'

default_app_config = 'apps.email_sender.Config'
