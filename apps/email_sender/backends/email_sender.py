from __future__ import unicode_literals
from __future__ import absolute_import


from django.core.mail.backends import smtp
from ..models import SMTPServer


class EmailSenderBackend(smtp.EmailBackend):
    smtp_server = None
    from_email = None
    reply_email = None

    def __init__(self, **kwargs):
        self.smtp_server = SMTPServer.objects.get_current()
        kwargs.update({
            'host': self.smtp_server.host, 'port': self.smtp_server.port,
            'username': self.smtp_server.username, 'password': self.smtp_server.password,
            'use_tls': self.smtp_server.use_tls, 'use_ssl': self.smtp_server.use_ssl, 'timeout': self.smtp_server.timeout,
            'ssl_keyfile': self.smtp_server.ssl_keyfile, 'ssl_certfile': self.smtp_server.ssl_keyfile
        })
        
        # torna atributos disponiveis publicamente
        self.from_email = self.smtp_server.from_email
        self.reply_to =  self.smtp_server.reply_to

        super(EmailSenderBackend, self).__init__(**kwargs)
        
        
