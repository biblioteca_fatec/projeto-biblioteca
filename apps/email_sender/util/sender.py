from __future__ import unicode_literals

from django.core import mail

from apps.email_sender.types import HTMLEmailMessage

import logging

# logger padrao
logger = logging.getLogger(__name__)


def send_html_message(subject, message, to,
                      template=None, context=None, plain_text_body=None,
                      from_email=None, cc=None, bcc=None, reply_to=None,
                      files=None, inline_images=None, attachments=None, headers=None):
    
    # create a connection to smtp server for reuse
    try:
        connection = mail.get_connection()
    except Exception as e:
        logger.error("Could get a mail connection")
        raise

    from_email = from_email or connection.from_email
    reply_to = reply_to or [connection.reply_to]

    # alteracao os3 pra enviar mensagem html
    email = HTMLEmailMessage(
        subject=subject, body=message, to=to,
        template=template, context=context, plain_text_body=plain_text_body,
        from_email=from_email, cc=cc, bcc=bcc, reply_to=reply_to,
        files=files, inline_images=inline_images, attachments=attachments, headers=headers,     
        connection=connection
    )

    logger.info("Sending to: %s" % to)
    return email.send()
