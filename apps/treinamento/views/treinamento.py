from cruds_adminlte.crud import CRUDView
from ..models import Treinamento
from ..forms import TreinamentoCreateForm, TreinamentoUpdateForm

class TreinamentoCRUDView(CRUDView):
    model = Treinamento
    add_form = TreinamentoCreateForm
    update_form = TreinamentoUpdateForm
    list_fields = ['id', 'titulo', 'resumo', 'criado_em', 'ativo']
    search_fields = ['resumo', 'titulo__icontains', 'criado_por']
    list_filter = ['ativo',]
    split_space_search = ' '
    paginate_by = 20

    def get_create_view(self):
        CreateView = super().get_create_view()

        class MyCreateView(CreateView):
            def get_form_kwargs(self):
                kwargs = super().get_form_kwargs()
                kwargs['user'] = self.request.user
                return kwargs

        return MyCreateView

    def get_detail_view(self):
        DetailView = super().get_detail_view()

        class MyDetailView(DetailView):
            self.template_name_base = 'treinamento/treinamento/cruds/detail.html'

        return MyDetailView

    # def get_list_view(self):
    #     ListView = super().get_list_view()
    #
    #     class MyListView(ListView):
    #         self.template_name_base = 'treinamento/treinamento/cruds/list.html'

        # return MyListView

    def get_update_view(self):
        UpdateView = super().get_update_view()

        class MyUpdateView(UpdateView):
            def get_form_kwargs(self):
                kwargs = super().get_form_kwargs()
                return kwargs

        return MyUpdateView

