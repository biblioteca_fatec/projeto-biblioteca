# Generated by Django 2.2.6 on 2019-10-29 19:45

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('treinamento', '0023_auto_20191029_1551'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='treinamento',
            options={'ordering': ['-criado_em'], 'verbose_name': 'Treinamento', 'verbose_name_plural': 'Treinamentos'},
        ),
        migrations.RemoveField(
            model_name='treinamento',
            name='visualizacao',
        ),
    ]
