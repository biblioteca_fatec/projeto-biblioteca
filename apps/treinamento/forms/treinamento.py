from django import forms
from django.utils.translation import ugettext_lazy as _
from ..models import Treinamento
from crispy_forms.layout import Layout, Field, Submit, Row
from crispy_forms.bootstrap import TabHolder, Tab, FormActions
from crispy_forms.helper import FormHelper
from ckeditor.widgets import CKEditorWidget


class TreinamentoCreateForm(forms.ModelForm):
    class Meta:
        model = Treinamento
        exclude = 'criado_em',
        widgets = {
            'file': forms.ClearableFileInput(attrs={'multiple':True})
        }


    def __init__(self, user, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = user
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Row(
                Field('titulo', wrapper_class="col-md-12"),
            ),
            Row(
                Field('resumo', wrapper_class="col-md-12"),
            ),
            Row(
                Field('video', wrapper_class="col-md-8"),
                Field('tamanho_video', wrapper_class="col-md-4"),
            ),
            Row(
                Field('imagem', wrapper_class="col-md-3"),
                Field('file', wrapper_class="col-md-3"),
                Field('file2', wrapper_class="col-md-3"),
                Field('file3', wrapper_class="col-md-3"),
            ),
            Row(
                Field('criado_por', wrapper_class="col-md-4"),
                Field('ordem', wrapper_class='col-md-4'),
                Field('ativo', wrapper_class="col-md-4"),
            ),

        )

        self.helper.layout.append(
            FormActions(
                Submit('submit', _('Salvar'), css_class='btn btn-primary'),
            )
        )

    def save(self, commit=True):
        obj = super().save(commit=False)
        obj.criado_por = self.user
        obj.save()
        self.save_m2m()
        return obj

class TreinamentoUpdateForm(forms.ModelForm):
    class Meta:
        model = Treinamento
        exclude = 'criado_em', 'criado_por'
        widgets = {
            'descricao': CKEditorWidget()
        }

    criado_por_nome = forms.CharField(label='Criado por', max_length=500, required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['criado_por_nome'].initial = self.instance.criado_por.__str__()
        self.fields['criado_por_nome'].widget.attrs.update({'disabled': 'disabled'})

        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Field('titulo', wrapper_class="col-md-12"),
            ),
            Row(
                Field('resumo', wrapper_class="col-md-12"),
            ),
            Row(
                Field('video', wrapper_class="col-md-8"),
                Field('tamanho_video', wrapper_class="col-md-4"),
            ),
            Row(
                Field('imagem', wrapper_class="col-md-3"),
                Field('file', wrapper_class="col-md-3"),
                Field('file2', wrapper_class="col-md-3"),
                Field('file3', wrapper_class="col-md-3"),
            ),
            Row(
                Field('ordem', wrapper_class='col-md-3'),
                Field('ativo', wrapper_class="col-md-3"),
            ),

        )

        self.helper.layout.append(
            FormActions(
                Submit('submit', _('Salvar'), css_class='btn btn-primary'),
            )
        )

        def save(self, commit=True):
            obj = super().save(commit=commit)
            self.save_m2m()
            return obj



