from django.db import models
from django.conf import settings
from django.utils.translation import gettext as _
from embed_video.fields import EmbedVideoField
#from ckeditor_uploader.fields import RichTextUploadingField
from django.core.validators import MinValueValidator, MaxValueValidator


class Treinamento(models.Model):

    tiny = _("tiny")
    small = _("small")
    medium = _("medium")
    large = _("large")
    huge = _("huge")

    TIPO_CHOICES = [
        (tiny, tiny),
        (small, small),
        (medium, medium),
        (large, large),
        (huge, huge),
    ]

    titulo = models.CharField(_("Título"), max_length=155)
    resumo = models.CharField(_('Resumo'), max_length=155, blank=False, null=False)
    criado_em = models.DateTimeField(_("Criado em"), auto_now_add=True)
    criado_por = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='treinamento_criado_por',
                                   verbose_name=_("Criado por"), on_delete=models.SET_NULL, null=True)
    ativo = models.BooleanField(_("Ativo"), default=False)
    file = models.FileField(_("Arquivo/Anexos"), upload_to='arquivos/', blank=True, null=True, default='')
    file2 = models.FileField(_("Arquivo/Anexos 2"), upload_to='arquivos/', blank=True, null=True, default='')
    file3 = models.FileField(_("Arquivo/Anexos 3"), upload_to='arquivos/', blank=True, null=True, default='')



    imagem = models.ImageField(_("Imagem de capa"), upload_to='media/imagens/', blank=False, null=False)

    video = EmbedVideoField(_("Vídeo Online (Youtube, Vimeo, etc.)"), null=True, blank=True,
                            help_text='Copie a URL do vídeo, ex: "https://www.youtube.com/watch?v=vmV8niW5GXs"')
    tamanho_video = models.CharField(_("Dimensão do Vídeo (Youtube/Vimeo"), max_length=255, null=False, blank=False,
                               choices=TIPO_CHOICES)

    #videofile = models.FileField(_("Enviar Vídeo (Arquivo)"), upload_to='videos/', null=True, blank=True,
                                 #help_text='Envie um vídeo diretamente de seu dispositivo')

    #poster_videofile = models.ImageField(_("Poste do vídeo"), upload_to='videos/poster/', null=True, blank=True)

    #tamanho_videofile = models.IntegerField(_("Dimensão do Vídeo - Mínimo: 420 e Máximo: 1280"), default=800,
                                      #validators=[MinValueValidator(420),
                                                  #MaxValueValidator(1280)])

    #descricao = RichTextUploadingField(_("Descrição"), blank=True, null=True)

    ordem = models.IntegerField(_("Ordem de Listagem"), validators=[MinValueValidator(1)], null=False, blank=False,
                                default=1)

    class Meta:
        verbose_name = _("Treinamento")
        verbose_name_plural = _("Treinamentos")

        ordering = ['ordem']

    def __str__(self):
        return self.titulo