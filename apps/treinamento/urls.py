from apps.core.utils import get_crud_urls_for
from .views import TreinamentoCRUDView


urlpatterns = get_crud_urls_for([
    TreinamentoCRUDView
])