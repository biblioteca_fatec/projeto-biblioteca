from django.urls import path
from .views import MensagemEmailPreView


urlpatterns = [
    path('mensagem-preview/', MensagemEmailPreView.as_view(), name='mensagem_preview'),
]
