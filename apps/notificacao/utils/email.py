from django.utils import timezone
from django.contrib.contenttypes.models import ContentType
from django.contrib.sites.models import Site
from apps.email_sender.util.sender import send_html_message
from ..models import MensagemEmail, AnexoMensagemEmail
import json
from datetime import date


def criar_mensagem(assunto, texto, para, template=None, context=None, de=None, obj=None, files=None):
    if not context:
        context = {}
    context.update({
        'mensagem': str(texto),
        'http': Site.objects.get_current().domain
    })
    mensagem = MensagemEmail(assunto=assunto, texto=texto, de=de, para=para, context=json.dumps(context))
    if template:
        mensagem.html = template
    if obj is not None:
        mensagem.content_type = ContentType.objects.get_for_model(obj)
        mensagem.object_id = obj.pk
    mensagem.criado_em = date.today()
    mensagem.save()
    if files:
        for file in files:
            AnexoMensagemEmail.objects.create(message=mensagem, anexo=file)
    return mensagem


def agendar_envio_mensagem(mensagem):
    if isinstance(mensagem, int):
        mensagem = MensagemEmail.objects.get(id=mensagem)
    mensagem.status = MensagemEmail.NA_FILA
    mensagem.envio_agendado_em = timezone.now()
    mensagem.save()
    return mensagem


def enviar_mensagem(mensagem):
    if isinstance(mensagem, int):
        mensagem = MensagemEmail.objects.get(id=mensagem)

    try:
        anexos = list()
        for mensagem_anexo in mensagem.anexos.all():
            anexos.append(mensagem_anexo.get_path)
        send_html_message(
            mensagem.assunto, mensagem.texto, [mensagem.para], from_email=mensagem.de, template=mensagem.html,
            context=mensagem.get_context, files=anexos)
        mensagem.status = mensagem.ENVIADO
        mensagem.log_envio = 'enviado com sucesso'
    except Exception as ex:
        mensagem.status = mensagem.ERRO
        mensagem.log_envio = ex
    
    mensagem.enviado_em = timezone.now()
    mensagem.save()

    return mensagem
