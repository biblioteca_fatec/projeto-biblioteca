from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import reverse
from django.contrib.postgres.fields import JSONField
import json


class MensagemEmailManager(models.Manager):
    def mensagens_do_objeto(self, obj):
        object_type = ContentType.objects.get_for_model(obj)
        return self.filter(content_type__pk=object_type.id, object_id=obj.pk)


class MensagemEmail(models.Model):
    objects = MensagemEmailManager()

    # status de envio
    CRIADO = 'Criado'
    NA_FILA = 'Na Fila'
    ENVIADO = 'Enviado'
    ERRO = 'Erro'

    STATUS_CHOICE = [
        (CRIADO, CRIADO),
        (NA_FILA, NA_FILA),
        (ENVIADO, ENVIADO),
        (ERRO, ERRO),
    ]

    class Meta:
        verbose_name = _('Mensagem de E-mail')
        verbose_name_plural = _('Mensagens de E-mail')
        ordering = ('-enviado_em', )

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, null=True)
    object_id = models.PositiveIntegerField(null=True)
    content_object = GenericForeignKey('content_type', 'object_id')

    assunto = models.CharField(
        _('Assunto'),
        max_length=400
    )

    html = models.TextField(
        _('Conteúdo HTML'), default='notificacao/email/base.html'
    )

    texto = models.TextField(
        _('Conteúdo Texto'), blank=True, null=True
    )

    criado_em = models.DateField(
        _('Criado em')
    )

    envio_agendado_em = models.DateTimeField(
        _('Enviado agendado em'), blank=True, null=True
    )

    enviado_em = models.DateTimeField(
        _('Enviado em'), blank=True, null=True
    )

    status = models.CharField(
        _('Status'),
        max_length=64,
        choices=STATUS_CHOICE,
        default=CRIADO
    )

    de = models.EmailField(_('De'), blank=True, null=True)

    para = models.EmailField(_('Para'))

    log_envio = models.TextField(
        _('Log de Envio'),
        blank=True, null=True
    )
    context = JSONField()

    def __str__(self):
        return self.assunto

    def get_absolute_url(self):
        return reverse('mensagem_email', kwargs={'pk': self.pk})

    @property
    def get_context(self):
        return json.loads(self.context)
