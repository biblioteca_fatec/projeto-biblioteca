from django.db import models
from django.utils.translation import ugettext_lazy as _


class TagMensagemEmail(models.Model):
    class Meta:
        verbose_name = ("Tag")
        verbose_name_plural = _("Tags")
   
    tag = models.CharField(
        'Tag', max_length=300, unique=True
    )

    def __str__(self):
        return self.tag

    @classmethod
    def get_or_create(cls, tag):
        o,f = cls.objects.get_or_create(tag=tag)
        return o