from django.db import models
from django.utils.translation import ugettext_lazy as _
from apps.core.utils import get_upload_path


class AnexoMensagemEmail(models.Model):
    class Meta:
        verbose_name = _("Anexo")
        verbose_name_plural = _("Anexos")
    
    message = models.ForeignKey(
        'notificacao.MensagemEmail',
        related_name='anexos',
        verbose_name='Mensagem',
        on_delete=models.PROTECT
    )
    anexo = models.FileField("Anexo", upload_to=get_upload_path)
    descricao = models.CharField("Descrição", max_length=300, default="", blank=True)

    def __str__(self):
        return "Anexo {}".format(self.id)

    @property
    def get_path(self):
        return self.anexo.path
