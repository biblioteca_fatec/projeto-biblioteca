from django.apps import AppConfig

VERSION = "0.1"
__version__ = VERSION


class Config(AppConfig):
    name = 'apps.notificacao'
    verbose_name = 'Notificação'

default_app_config = 'apps.notificacao.Config'