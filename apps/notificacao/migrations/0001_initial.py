# Generated by Django 2.2.4 on 2019-08-13 12:02

import apps.core.utils.filepath
import django.contrib.postgres.fields.jsonb
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='TagMensagemEmail',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tag', models.CharField(max_length=300, unique=True, verbose_name='Tag')),
            ],
            options={
                'verbose_name': 'Tag',
                'verbose_name_plural': 'Tags',
            },
        ),
        migrations.CreateModel(
            name='MensagemEmail',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('object_id', models.PositiveIntegerField(null=True)),
                ('assunto', models.CharField(max_length=400, verbose_name='Assunto')),
                ('html', models.TextField(default='notificacao/email/base.html', verbose_name='Conteúdo HTML')),
                ('texto', models.TextField(blank=True, null=True, verbose_name='Conteúdo Texto')),
                ('criado_em', models.DateField(verbose_name='Criado em')),
                ('envio_agendado_em', models.DateTimeField(blank=True, null=True, verbose_name='Enviado agendado em')),
                ('enviado_em', models.DateTimeField(blank=True, null=True, verbose_name='Enviado em')),
                ('status', models.CharField(choices=[('Criado', 'Criado'), ('Na Fila', 'Na Fila'), ('Enviado', 'Enviado'), ('Erro', 'Erro')], default='Criado', max_length=64, verbose_name='Status')),
                ('de', models.EmailField(blank=True, max_length=254, null=True, verbose_name='De')),
                ('para', models.EmailField(max_length=254, verbose_name='Para')),
                ('log_envio', models.TextField(blank=True, null=True, verbose_name='Log de Envio')),
                ('context', django.contrib.postgres.fields.jsonb.JSONField()),
                ('content_type', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='contenttypes.ContentType')),
            ],
            options={
                'verbose_name': 'Mensagem de E-mail',
                'verbose_name_plural': 'Mensagens de E-mail',
                'ordering': ('-enviado_em',),
            },
        ),
        migrations.CreateModel(
            name='AnexoMensagemEmail',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('anexo', models.FileField(upload_to=apps.core.utils.filepath.get_upload_path, verbose_name='Anexo')),
                ('descricao', models.CharField(blank=True, default='', max_length=300, verbose_name='Descrição')),
                ('message', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='anexos', to='notificacao.MensagemEmail', verbose_name='Mensagem')),
            ],
            options={
                'verbose_name': 'Anexo',
                'verbose_name_plural': 'Anexos',
            },
        ),
    ]
