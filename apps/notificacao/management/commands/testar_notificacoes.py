from django.core.management.base import BaseCommand
from django.utils import timezone

from apps.notificacao.tasks import (
    do_agendar_envio_mensagens_email,
    do_enviar_mensagens_email
)

class Command(BaseCommand):
    help = 'Teste de Notificao'

    def handle(self, *args, **kwargs):
        do_agendar_envio_mensagens_email()
        self.stdout.write("agendamento finalizado")

        do_enviar_mensagens_email()
        self.stdout.write("envio finalizado")

        self.stdout.write("teste de notificacoes finalizados")