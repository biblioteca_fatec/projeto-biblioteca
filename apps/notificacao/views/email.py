from __future__ import unicode_literals

from django.views.generic import TemplateView
from django.shortcuts import get_object_or_404
from django.contrib.sites.models import Site

from ..models import MensagemEmail

class MensagemEmailPreView(TemplateView):
    template_name = "notificacao/email/base.html"
    
    def get_context_data(self, *args, **kwargs):
        notificacao = MensagemEmail.objects.all().first()
        
        if notificacao is not None:
            kwargs.update(notificacao.get_context)
            kwargs.update({'http':''})
        return kwargs
