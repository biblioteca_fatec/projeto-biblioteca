from __future__ import absolute_import, unicode_literals
from celery.utils.log import get_task_logger
from celery.task import task, periodic_task
from apps.notificacao.utils import email as notificao_email
from apps.notificacao.models import MensagemEmail
from apps.core.utils import memcache_lock
from datetime import timedelta

logger = get_task_logger(__name__)

######################
# AGENDAMENTOS

def do_agendar_envio_mensagens_email():
    mensagens = MensagemEmail.objects.filter(status=MensagemEmail.CRIADO)
    for mensagem in mensagens:
        notificao_email.agendar_envio_mensagem(mensagem)


@periodic_task(name="agendar_envio_mensagens_email", run_every=timedelta(seconds=5), bind=True)
def agendar_envio_mensagens_email(self):
    with memcache_lock(self.name, self.app.oid) as acquired:
        if acquired:
            do_agendar_envio_mensagens_email()
        return 'processado'


##########################
# ENVIOS

def do_enviar_mensagens_email():
    mensagens = MensagemEmail.objects.filter(status=MensagemEmail.NA_FILA)
    for mensagem in mensagens:
        notificao_email.enviar_mensagem(mensagem)

@periodic_task(name="enviar_mensagens_email", run_every=timedelta(seconds=10), bind=True)
def enviar_mensagens_email(self):
    with memcache_lock(self.name, self.app.oid) as acquired:
        if acquired:
            do_enviar_mensagens_email()
        return 'processado'


