from django.utils.translation import ugettext_lazy as _
from ..models import Autor
from django import forms

class AutorBaseForm(forms.ModelForm):
    class Meta:
        model = Autor
        fields = '__all__'

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.fields['autor'].required = True
