from django import forms
from ..models import Livro
from django.utils.translation import ugettext_lazy as _
from crispy_forms.layout import Layout, Field, Submit, Row
from crispy_forms.bootstrap import TabHolder, Tab, FormActions
from crispy_forms.helper import FormHelper

class LivroForm(forms.ModelForm):
    class Meta:
        model = Livro
        fields = ['titulo', 'autor', 'editora', 'paginas', 'ativo']

    def __init__(self, user, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = user
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Row(
                Field('titulo', wrapper_class="col-md-12"),
            ),
            Row(
                Field('autor', wrapper_class="col-md-6"),
                Field('editora', wrapper_class="col-md-6"),
            ),
            Row(
                Field('paginas', wrapper_class="col-md-4"),
                Field('ativo', wrapper_class="col-md-4"),
            ),
        )

        self.helper.layout.append(
            FormActions(
                Submit('submit', _('Salvar'), css_class='btn btn-primary'),
            )
        )

    def save(self, commit=True):
        obj = super().save(commit=False)
        obj.criado_por = self.user
        obj.save()
        self.save_m2m()
        return obj