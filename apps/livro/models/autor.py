from django.db import models
from django.utils.translation import ugettext_lazy as _

class Autor(models.Model):

    class Meta:
        verbose_name = _('Autor')
        verbose_name_plural = _('Autores')
        ordering = ('nome',)

    nome = models.CharField(_("Nome"), max_length=50, blank=True, null=True)
    sobrenome = models.CharField(_("Sobre Nome"), max_length=150, blank=True, null=True)

    def __str__(self):
        return self.nome