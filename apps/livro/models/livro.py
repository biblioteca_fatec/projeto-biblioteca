from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _


class Livro(models.Model):

    class Meta:
        verbose_name = _(u'Livro')
        verbose_name_plural = _('Livros')
        ordering = ('titulo',)

    titulo = models.CharField(_("Título"), max_length=255, null=False, blank=False)
    autor = models.ForeignKey("Autor", on_delete=models.CASCADE)
    editora = models.CharField(_("Editora"), max_length=255, null=True, blank=True)
    paginas = models.IntegerField(_("Número de páginas"), null=False, blank=False)

    criado_em = models.DateTimeField(_("Criado em"), auto_now_add=True)
    criado_por = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='livro_criado_por',
                                   verbose_name=_("Criado por"), on_delete=models.SET_NULL, null=True)

    ativo = models.BooleanField(_('Ativo'), default=False)

    def __str__(self):
        return self.titulo
