from cruds_adminlte.crud import CRUDView
from ..models import Livro
from ..forms import LivroForm

class LivroCRUDView(CRUDView):
    model = Livro
    add_form = LivroForm
    list_fields = ['id', 'titulo', 'autor', 'criado_em', 'criado_por', 'editora', 'ativo',]
    search_fields = ['titulo__icontains']
    list_filter = ['ativo',]
    split_space_search = ' '
    paginate_by = 20

    def get_create_view(self):
        CreateView = super().get_create_view()

        class MyCreateView(CreateView):
            def get_form_kwargs(self):
                kwargs = super().get_form_kwargs()
                kwargs['user'] = self.request.user
                return kwargs

        return MyCreateView


