from cruds_adminlte.crud import CRUDView
from ..models import Autor
from ..forms import AutorBaseForm

class AutorCRUDView(CRUDView):
    model = Autor
    add_form = AutorBaseForm
    views_available = ['list', 'create', 'delete', 'update', 'detail']
    list_fields = ['nome', 'sobrenome']