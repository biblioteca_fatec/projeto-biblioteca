from apps.core.utils import get_crud_urls_for
from .views import LivroCRUDView, AutorCRUDView


urlpatterns = get_crud_urls_for([
    LivroCRUDView, AutorCRUDView
])